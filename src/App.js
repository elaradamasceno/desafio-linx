import React, { Component } from 'react';
import './css/pure-min.css';
import './css/side-menu.css';
import './css/menu-app.css';

import $ from 'jquery'; 

class App extends Component {
    constructor(){
        super();
        this.state = {
            pontos : 10,
            saldo: 'R$ 450,00',
            valueDoc: '',
            numConsulta: 0
        }

        this.handleChangeNumberDoc = this.handleChangeNumberDoc.bind(this);
    }

    handleChangeNumberDoc(event){
        this.setState({ valueDoc: event.target.value });
    }

    onSubmit(e){
        e.preventDefault();
        // const url = 'http://localhost:1337/consulta';

        const url = 'https://hfllinxintegracaogiftwebapi-hom.azurewebsites.net/LinxServiceApi/FidelidadeService/ConsultaFidelizacao';

        $.ajax({
            type: "POST", 
            methodType: "POST",
            url: url,
            timeout: 3000,
            contentType: "application/json",
            cache: false,
            dataType: "jsonp",
            data : JSON.stringify({
                chaveIntegracao: "4B335B6F-9C4D-47F7-B798-C46FFBC4881A",
                codigoLoja: 1,
                numeroCartao: 32231126850,
                nsuCliente: -1,
                codigoSeguranca: 32231126850
            }),
            error: function() {
               console.log('erro')
            },
            success: function(retorno) {
                console.log(retorno)
            } 
        });  

        // $.ajax({
        //     url: url,
        //     dataType: "jsonp",
        //     // method: "POST",
        //     type: "POST",
        //     jsonpCallback: 'processJSONPResponse',
        //     contentType: "application/json; charset=utf-8",
        //     data : {
        //         card_number: 32231126850
        //     },
        //     success: function(result) {
        //       console.log(result);
        //     },
        //     error: function(error) {
        //       console.log(error)
        //     }
        //   });
    }

    render() {
        return (
            <div id="layout">
                <div id="menu" className="menu-app">
                    <div className="pure-menu">
                        <span className="pure-menu-heading color-heading">Linx</span>
                    </div>
                </div>

                <div id="main">
                    <div className="header">
                        <h1>Consultar Pontos de Fidelidade</h1>
                    </div>
                    <div className="content" id="content">
                        <div className="pure-form pure-form-aligned">
                            <form className="pure-form pure-form-aligned">
                                {/* <span>Informe o seguinte dado para consulta</span> */}
                                <div className="pure-control-group">
                                    <label htmlFor="numberDoc">Número do Documento</label> 
                                    <input id="numberDoc" type="text" name="number" value={this.state.value} onChange={this.handleChangeNumberDoc}/>                  
                                </div>
                                <div className="pure-control-group">                                  
                                    <label></label> 
                                    <button type="submit" className="pure-button pure-button-primary" onClick={this.onSubmit.bind(this)}>Consultar</button>                                    
                                </div>
                            </form>            
                        </div>  
                        <div>            
                            <table className="pure-table">
                                <thead>
                                    <tr>
                                        <th>Saldo R$</th>
                                        <th>Qtd. Pontos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{this.state.saldo}</td>
                                        <td>{this.state.pontos}</td>                             
                                    </tr>
                                </tbody>
                            </table> 
                        </div>             
                    </div>
                </div>            
            </div>     
        );
    } 
}

export default App;